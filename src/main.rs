#![feature(async_closure, inner_deref, try_trait)]
#![allow(clippy::type_complexity)]

mod uses;
use uses::*;

#[macro_use]
mod auth;
#[macro_use]
mod util;
mod result;
mod voters;

lazy_static! {
    pub static ref SETTINGS: Settings = init();
    pub static ref STATE: State = Mutex::new(Data::default());
    pub static ref TOKENS: Tokens = gen_tokens(&SETTINGS);
}

#[async_std::main]
async fn main() -> Fallible<()> {
    let mut app = tide::new();

    app.middleware(auth_middleware);
    app.middleware(RequestLogger::new());

    app.at("/auth")
        .post(post_auth)
        .get(get_auth);

    let mut voters = tide::new();
    voters.at("/:id")
        .put(put_vote)
        .get(get_vote)
        .delete(delete_vote);
    voters.at("/").get(get_voters);

    app.at("/voters").nest(voters);

    app.at("/result")
        .get(get_result)
        .post(post_result);

    app.at("/candidates").get(async move |_| json(&SETTINGS.candidates));

    app.at("/name").get(async move |_| json(&SETTINGS.name));

    app.listen(&SETTINGS.socket).await?;
    Ok(())
}

fn init() -> Settings {

    let mut file = vec![];

    let matches = clap_app!{voting =>
        (version: "0.1.0")
        (author: "Steven Xu <stevendoesstuffs@protonmail.com>")
        (@arg config: -c --config +takes_value +global default_value("Settings.toml") [FILE]
            "Set the settings file")
        (@subcommand genkey =>
            (about: "Generate a JWT key")
            (version: "1.0.0")
            (author: "Steven Xu <stevendoesstuffs@protonmail.com>")
        )
        (@subcommand signid =>
            (about: "Sign a range of IDs with the key in config output it into a pattern")
            (version: "1.0.0")
            (author: "Steven Xu <stevendoesstuffs@protonmail.com>")
        )
    }.get_matches();

    File::open(matches.value_of("config").unwrap())
        .expect("Error opening config")
        .read_to_end(&mut file).expect("Error parsing config");
    // output

    let mut settings: Settings = toml::from_slice(&file).expect("Error parsing config");

    if let (command, Some(_)) = matches.subcommand() {
        match command {
            "genkey" => {
                let mut jwt_key = [0u8; 32];
                thread_rng().fill_bytes(&mut jwt_key);
                println!("{}", base64::encode(&jwt_key));
                exit(0);
            },
            "signid" => {
                let Tokens{from_id: vec, ..} = gen_tokens(&settings);
                for i in 0..vec.len() {
                    println!("{} - {}", base64::encode(&vec[i]), i);
                }
                exit(0);
            }, _ => {}
        }
    }

    for i in 0..settings.candidates.len() {
        settings.candidates[i].id = i as u32;
    }

    settings
}
