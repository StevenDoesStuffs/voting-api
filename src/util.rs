use crate::uses::*;

#[derive(Default)]
pub struct Data {
    pub voters: HashMap<u32, Vec<Vec<u32>>>,
    pub results: Option<VoteResult>
}

pub struct Tokens {
    pub from_token: HashMap<Vec<u8>, u32>,
    pub from_id: Vec<Vec<u8>>
}

pub type State = Mutex<Data>;

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub name: String,
    pub socket: String,
    #[serde(deserialize_with = "from_base64")]
    pub key: [u8; 32],
    pub num_voters: u32,
    pub token_bytes: u32,
    pub invalid_ids: Vec<u32>,
    pub candidates: Vec<Candidate>,
    pub testing: bool
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Candidate {
    #[serde(skip_deserializing)]
    pub id: u32,
    pub name: String,
    pub desc: String
}


fn from_base64<'de, D>(deserializer: D) -> Result<[u8; 32], D::Error>
        where D: Deserializer<'de> {

    use serde::de::Error;
    String::deserialize(deserializer)
        .and_then(|string| base64::decode(&string)
            .map_err(|err| Error::custom(err.to_string()))
            .and_then(|vec| if vec.len() == 32 {
                let mut array = [0_u8; 32];
                array.copy_from_slice(&vec);
                Ok(array)
            } else {
                Err(Error::custom("Value must be 256 bits/32 bytes"))
            })
        )
}

pub fn json(data: &impl Serialize) -> Response {
    Response::new(200).body_json(data).unwrap()
}

#[macro_export]
macro_rules! r {
    ($err: expr, $value: expr) => {
        match $value {
            Ok(x) => x,
            Err(_) => return Response::new($err)
        }
    };
}

#[macro_export]
macro_rules! ok {
    () => {
        Response::new(200)
    };
}
