pub use crate::uses::*;


#[derive(Serialize, Deserialize)]
pub struct VoteResult {
    places: Vec<u32>,
    pairs: Vec<Vec<u32>>,
    paths: Vec<Vec<u32>>,
    wins: Vec<u32>
}

pub fn compute_result(pairs: Vec<Vec<u32>>) -> VoteResult {
    let n = pairs.len();
    let mut paths = pairs.clone();

    // initialize p to all known widest paths
    for i in 0..n {
        for j in 0..n {
            if i != j && pairs[i][j] <= pairs[j][i] {
                paths[i][j] = 0;
            }
        }
    }

    // floyd warshall for widest paths
    for k in 0..n {
        for i in 0..n {
            if i == k {continue}
            for j in 0..n {
                if j == i || j == k {continue}
                paths[i][j] = max(paths[i][j], min(paths[i][k], paths[k][j]));
            }
        }
    }

    let mut wins = vec![0; n];
    let mut places = (0..n as u32).collect::<Vec<_>>();

    for i in 0..n {
        let mut count = 0;
        for j in 0..n {
            if i == j {continue}
            if paths[i][j] > paths[j][i] {count += 1}
        }
        wins[i] = count;
    }

    places.sort_unstable_by_key(|i| wins[*i as usize]);
    places.reverse();

    VoteResult{places, paths, pairs, wins}
}

pub async fn get_result(_: Request<()>) -> Response {
    let state = STATE.lock().await;

    match &state.results {
        Some(results) => json(results),
        None => Response::new(403)
    }
}

pub async fn post_result(mut req: Request<()>) -> Response {
    check_user!{req, 0};

    let show_results: bool = r!{400, req.body_json().await};
    let mut state = STATE.lock().await;

    state.results = if show_results {
        let votes = state.voters.values();
        let n = SETTINGS.candidates.len();
        let mut pairs: Vec<Vec<u32>> = vec![vec![0; n]; n];

        for vote in votes {
            let mut hi: Vec<u32> = vec![];
            for group in vote {
                for v in group {
                    for h in &hi {
                        pairs[*h as usize][*v as usize] += 1;
                    }
                }
                hi.extend_from_slice(group);
            }
        }

        Some(compute_result(pairs))
    } else {None};

    ok!()
}
