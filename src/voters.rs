use crate::uses::*;

pub async fn put_vote(mut req: Request<()>) -> Response {
    let id: u32 = r!{400, req.param("id")};
    check_user!{req, id};

    let vote: Vec<Vec<u32>> = r!{400, req.body_json().await};
    let mut state = STATE.lock().await;
    // admin cant vote, cant vote past finish
    if id == 0 || state.results.is_some() {
        return Response::new(403);
    }

    // validation
    let n = SETTINGS.candidates.len();
    let mut count = 0;
    let mut counted = vec![false; n];

    for group in &vote {
        for v in group {
            let v = *v as usize;
            if v >= n || counted[v] {
                return Response::new(400);
            }
            counted[v] = true;
            count += 1;
        }
    }
    if count != n {
        return Response::new(400);
    }

    state.voters.insert(id, vote);

    ok!()
}

pub async fn get_vote(req: Request<()>) -> Response {
    let id: u32 = r!{400, req.param("id")};
    let state = STATE.lock().await;
    if state.results.is_none() {
        check_user!{req, id};
    }

    match state.voters.get(&id) {
        None => Response::new(404),
        Some(list) => json(list)
    }
}

pub async fn delete_vote(req: Request<()>) -> Response {
    let id: u32 = r!{400, req.param("id")};
    check_user!{req, id};

    let mut state = STATE.lock().await;
    match state.voters.remove(&id) {
        Some(..) => ok!(),
        None => Response::new(404)
    }
}

pub async fn get_voters(_: Request<()>) -> Response {
    let state = STATE.lock().await;

    match &state.results {
        Some(..) => json(&state.voters),
        None => Response::new(403)
    }
}
