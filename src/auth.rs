use crate::uses::*;

struct LoginData(Option<u32>);

pub fn auth_middleware<'a, T: SSS>(req: Request<T>, next: Next<'a, T>) ->
        BoxFuture<'a, Response> {Box::pin(async move {

    let login_data = LoginData(if let Some(cookie) = req.cookie("auth").unwrap() {
        let decoded = percent_decode_str(cookie.value()).decode_utf8().ok();
        decoded
            .and_then(|s| base64::decode(&*s).ok())
            .and_then(|token|
                TOKENS.from_token.get(&token).map(|x| *x))
    } else {
        None
    });

    next.run(req.set_local(login_data)).await
})}

pub trait AuthRequest {
    fn get_login_data(&self) -> Option<u32>;
}

impl<T: SSS> AuthRequest for Request<T> {
    fn get_login_data(&self) -> Option<u32> {
        if let Some(login) = self.local::<LoginData>() {
            login.0
        } else {
            eprintln!("Could not do authentication due: missing AuthMiddleware");
            None
        }
    }
}

pub async fn post_auth(mut req: Request<()>) -> Response {
    let token_str = r!{400, req.body_json::<String>().await};
    let token = r!{400, base64::decode(&token_str)};
    if let Some(_) = TOKENS.from_token.get(&token) {
        let mut response = ok!();
        response.set_cookie(Cookie::build("auth", token_str)
            .permanent()
            .http_only(true)
            .finish());
        response
    } else {
        // TODO: limit number of attempts possible
        Response::new(401)
    }
}

pub async fn get_auth(req: Request<()>) -> Response {
    if let Some(id) = req.get_login_data() {
        json(&id)
    } else {
        Response::new(401)
    }
}

pub fn gen_tokens(settings: &Settings) -> Tokens {
    let mut rng = ChaChaRng::from_seed(settings.key);
    let mut vec = vec![];
    let mut map = HashMap::default();

    for _ in 0..=settings.num_voters {
        let mut token = vec![0; settings.token_bytes as usize];
        rng.fill_bytes(&mut token);
        map.insert(token.clone(), vec.len() as u32);
        vec.push(token);
    }

    Tokens {
        from_id: vec,
        from_token: map
    }
}

#[macro_export]
macro_rules! check_user {
    ($req: expr, $user: expr) => {
        if let Some(id) = $req.get_login_data() {
            if id != $user {
                return Response::new(403);
            }
        } else {
            return Response::new(401);
        }
    };
}
