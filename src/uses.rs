pub use failure::{format_err, Fallible, Error};

pub use std::{
    collections::{
        HashMap, HashSet
    },
    convert::identity,
    fs::File,
    io::Read,
    process::exit,
    cmp::{Ordering, min, max}
};

pub use tide::{
    prelude::*,
    new,
    Result as TResult,
    middleware::{Middleware, Next, RequestLogger},
    Request,
    Response
};
pub use http::{
    status::StatusCode,
    header,
};
pub use percent_encoding::percent_decode_str;

pub use async_std::{
    prelude::*,
    sync::Mutex
};
pub use futures::future::BoxFuture;

pub use serde::{Serialize, Deserialize, Deserializer};
pub use lazy_static::lazy_static;
pub use rand::prelude::*;
pub use rand_chacha::ChaChaRng;
pub use clap::clap_app;
pub use cookie::Cookie;

pub trait SSS: Send + Sync + 'static {}
impl<T> SSS for T where T: Send + Sync + 'static {}

pub use crate::*;
pub use crate::util::*;
pub use crate::auth::*;
pub use crate::result::*;
pub use crate::voters::*;
